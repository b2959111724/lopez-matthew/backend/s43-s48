
//[SECTION] Dependencies and Modules
	const express = require("express");
	const courseController = require("../controllers/course");
	const auth = require("../auth") 

	const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
	const router = express.Router();





//[SECTION] create a course POST - Create
router.post("/", verify, verifyAdmin, courseController.addCourse);

// [SECTION] Route for retrieving all the courses (Admin)
router.get("/all", verify, verifyAdmin, courseController.getAllCourses);


// [SECTION] Route for retrieving all the ACTIVE courses for all the users.
router.get("/", courseController.getAllActive);


// Route to search for courses by course name
router.post('/search', courseController.searchCoursesByName);

//[ACTIVITY] Search Courses By Price Range
router.post('/searchByPrice', courseController.searchCoursesByPriceRange);


// [SECTION] Route for retrieving a specific course
// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url not static and may change depending on the information provided in the url.
router.get("/:courseId", courseController.getCourse);


// [SECTION] Route for updating a course (Admin)
// add JWT authentication for verifying if the user is an admin or not. Admin will only have access to update a course.
router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);


// [ACTIVITY] Route to archiving a course (Admin)
// A "PUT" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse);


//[ACTIVITY] Route to activating a course (Admin)
router.put("/:courseId/activate", verify, verifyAdmin, courseController.activateCourse);



//sample route
router.get('/:courseId/enrolled-users', courseController.getEmailsOfEnrolledUsers);



// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;


